#!/usr/bin/slsh
% A simple mutt query script.  Unlike rolo's mutt_vc_query,
% this one does a regexp match.
import("vcard");

define vc_get_field(vc, field)
{
   variable f = vc_get_next_by_name(vc, field);
   if (f != NULL)
     f = vc_get_value(f);
   else f = "";
   return f;
}

define main(argc, argv)
{
   if (argc != 2)
     return message("usage: mutt_query <regexp>");
   variable needle = argv[1];
   variable vfp=fopen(path_concat (getenv("HOME"),".rolo/contacts.vcf"), "r");
   variable vc, name, email, nick, found = 0;
   for (vc = parse_vcard_file(vfp); vc != NULL; vc = parse_vcard_file(vfp))
     {
	name = vc_get_field (vc, VC_FORMATTED_NAME);
	nick = vc_get_field (vc, VC_NICKNAME);
	email = vc_get_preferred_email (vc);

	if (email==NULL) continue;

	if (orelse
	    {string_match(name, needle, 1)}
	    {string_match(email, needle, 1)}
	    {string_match(nick, needle, 1)})
	  {
	     !if (found) 
	       {
		  printf("\n");
		  found = 1;
	       }
	     printf("%S\t%S\t%S\n", email, name, nick);
	  }
     }
   !if (found)
     printf("Not found\n");
}

main(__argc, __argv);
