CC_SHARED = gcc $(CFLAGS) -shared -fPIC
SLANG_INC = -I/usr/include
SLANG_LIB = -L/usr/lib -lslang -L/usr/local/lib -lvc

LIBS = $(SLANG_LIB) -lm
INCS = $(SLANG_INC)

all: vcard-module.so

vcard-module.so: *.c *.h
	$(CC_SHARED) $(INCS) -O2 vcard-module.c -o vcard-module.so $(LIBS)

vcard-module.o: *.c *.h
	$(CC_SHARED) $(INCS) -O2 -c vcard-module.c -o vcard-module.o

clean:
	rm -f vcard-module.so  *.o
